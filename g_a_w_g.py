
# Програма для гри в "козу та вовків" з комп"ютером

import re
import sys
import random
import MySQLdb
import goat_and_wolves
import settings
import goat_messages


HUMANS_MOVE = 'humans_move'
COMPUTERS_MOVE = 'computers_move'
INDEX_LITERAL_OLD_COORDINATE_IN_STRING_COORDINATES = 0
INDEX_NUMERAL_OLD_COORDINATE_IN_STRING_COORDINATES = 1
INDEX_LITERAL_NEW_COORDINATE_IN_STRING_COORDINATES = 3
INDEX_NUMERAL_NEW_COORDINATE_IN_STRING_COORDINATES = 4
INTEGER_FIRST_RESULT_OF_QUESTION = 0
INTEGER_SECOND_RESULT_OF_QUESTION = 1
LITERAL_NUMBER = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7}
NUMBER_TO_LITERAL = {1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G', 8: 'H'}


def convert_string(string_of_position):
    # функція приміняється в модулі web_game.py, та логічно її розмістити тут до подібних функцій, щоб не розривати
    # логічного компонування функцій
    # повертає словник, де ключ це координата клітинки (у вигляді 'А8'), а дані це те що знаходиться на клітинці, тільки
    # якщо вона не порожня (у вигляді 'w')
    coordinates_checkers_on_board = {}
    position = goat_and_wolves.string_to_position(string_of_position)
    board = position[goat_and_wolves.INDEX_FOR_BOARD]
    index_row = 0
    for row in board:
        index_row += 1
        index_square = 0
        for square in row:
            index_square += 1
            if square != goat_and_wolves.EMPTY_SQUARE:
                coordinates_checkers_on_board[(NUMBER_TO_LITERAL[index_square] + str(index_row))] = (square, )
    return coordinates_checkers_on_board


def instruction():
    print(goat_messages.INSTRUCTION)


def open_db_of_moves():
    db_connect = MySQLdb.connect(host=settings.DB_HOST,
                                 user=settings.DB_USERNAME,
                                 passwd=settings.DB_PASSWORD,
                                 db=settings.DB_NAME)
    cursor_db = db_connect.cursor()
    return cursor_db, db_connect


def close_db_of_moves(db_for_close):
    db_for_close.close()


def input_moves():
    # просить ввести хід (у вигляді H8-G7) до тих пір поки оператор не введе правильно повертає строку ходу
    while True:
        move = input(goat_messages.YOUR_MOVE).upper().strip().replace(' ', '')
        scheme = re.compile(r'[A-H][1-8][-,.:;=][A-H][1-8]$')
        if re.match(scheme, move):
            return move
        else:
            print(goat_messages.CHECK_YOUR_MOVE)


def piece_of_humans_move(string_position, move=None):
    # приймає позицію у вигляді строки, та хід у вигляді F8(один із розділових знаків-,.:;=)G7 повертає назад строку
    # зновою позицією та None, якщо хід відбувся, або False та строку повідомлення, якщо хід не відбувся.
    old_coordinates, new_coordinates = transforming_coordinates(move)
    move_humans, message = making_move(old_coordinates, new_coordinates, string_position)
    return move_humans, message


def humans_move(string_position):
    # фунуція циклічно визиває функцію вводу ходу до тих пір поки оператор не введе правильно хід та повертає строку
    # позиції
    move = None
    while move is None:
        move = input_moves()
        move_humans, message = piece_of_humans_move(string_position, move)
        if not move_humans:
            print(message)
            move = None
        else:
            return move_humans


def computers_move(string_position):
    # З можливих ходів вибирає в базі даних хід, який призводить до перемоги, або вираховує найбільш оптимальний
    # і повертає його.
    cursor, db = open_db_of_moves()
    win_moves = []
    high_priority_moves = []
    secondary_priority_moves = []
    possible_moves = goat_and_wolves.get_possible_moves(string_position)
    for move in possible_moves:
        if evaluation_priority_move(string_position, move, goat_and_wolves.WHITE_CHECKER):
            secondary_priority_moves.append(move)
        if cursor.execute((
            goat_messages.SELECT_DATA.format(position=move))) and \
                cursor.fetchone()[0] == goat_and_wolves.LOSS_FOR_MOVING_SIDE:
            win_moves.append(move)
            if move[goat_and_wolves.INDEX_IN_STRING_FOR_COLOR_MOVES] == goat_and_wolves.BLACK_MOVES and \
                    evaluation_priority_move(string_position, move, goat_and_wolves.WHITE_CHECKER):
                high_priority_moves.append(move)
    move_computers = random.choice(high_priority_moves or win_moves or secondary_priority_moves or possible_moves)
    close_db_of_moves(db)
    return move_computers


def evaluation_priority_move(position, new_position, checker_color):
    old_move_index = goat_and_wolves.checkers_index(position, checker_color)
    new_move_index = goat_and_wolves.checkers_index(new_position, checker_color)
    return True if new_move_index < old_move_index else False


def transforming_coordinates(input_string):
    old_x = LITERAL_NUMBER[input_string[INDEX_LITERAL_OLD_COORDINATE_IN_STRING_COORDINATES]]
    old_y = int(input_string[INDEX_NUMERAL_OLD_COORDINATE_IN_STRING_COORDINATES]) - 1
    new_x = LITERAL_NUMBER[input_string[INDEX_LITERAL_NEW_COORDINATE_IN_STRING_COORDINATES]]
    new_y = int(input_string[INDEX_NUMERAL_NEW_COORDINATE_IN_STRING_COORDINATES]) - 1
    return (old_x, old_y), (new_x, new_y)


def making_move(old_coordinates, new_coordinates, string_position):
    # функція перевіряє чи на клітинці із старими координатами знаходиться шашка того кольору чий хід, чи вона при зміні
    # на нові координати не виходить за межі ігрового поля та чи ходить за дозволеними правилами, відповідно виконує
    # хід, тобто змінює позицію та міняє індикатор кольору чий хід.
    position = goat_and_wolves.string_to_position(string_position)
    old_x, old_y = old_coordinates
    new_x, new_y = new_coordinates
    board = position[goat_and_wolves.INDEX_FOR_BOARD]
    who_moves = position[goat_and_wolves.INDEX_FOR_COLOR_MOVES]
    directions = goat_and_wolves.WHITE_DIRECTIONS if \
        who_moves == goat_and_wolves.WHITE_MOVES else goat_and_wolves.BLACK_DIRECTIONS
    possible_coordinates = goat_and_wolves.get_checker_moves(old_x, old_y, directions)
    if board[new_y][new_x] != goat_and_wolves.EMPTY_SQUARE or new_coordinates not in possible_coordinates:
        return False, goat_messages.IMPOSSIBLE_MOVE
    elif board[old_y][old_x] != who_moves.lower():
        return False, goat_messages.CHECK_CHECKER
    board[old_y][old_x] = goat_and_wolves.EMPTY_SQUARE
    board[new_y][new_x] = who_moves.lower()
    who_moves = goat_and_wolves.BLACK_MOVES if who_moves == goat_and_wolves.WHITE_MOVES else goat_and_wolves.WHITE_MOVES
    return goat_and_wolves.position_to_string([board, who_moves]), None


def positions_evaluation(position):
    # Перевіряє позицію на наявність виграшу, та повертає колір шашки, які виграли.
    if position[goat_and_wolves.INDEX_IN_STRING_FOR_COLOR_MOVES] == goat_and_wolves.BLACK_MOVES:
        white_index = goat_and_wolves.checkers_index(position, goat_and_wolves.WHITE_CHECKER)
        black_index = goat_and_wolves.checkers_index(position, goat_and_wolves.BLACK_CHECKER)
        if white_index <= black_index:
            return goat_and_wolves.WHITE_CHECKER
    elif position[goat_and_wolves.INDEX_IN_STRING_FOR_COLOR_MOVES] == goat_and_wolves.WHITE_MOVES:
        if not goat_and_wolves.get_possible_moves(position):
            return goat_and_wolves.BLACK_CHECKER
    else:
        return False


def print_board(position, color):
    position_for_print = goat_and_wolves.make_empty_board(goat_and_wolves.SQUARE, goat_and_wolves.ROW)
    counter = 0
    for row in range(len(position_for_print)):
        for square in range(len(position_for_print[row])):
            if row % 2 == square % 2:
                position_for_print[row][square] = position[counter]
            else:
                position_for_print[row][square] = '-'
            counter += 1
    if color == goat_and_wolves.WHITE_CHECKER:
        index = goat_and_wolves.ROW
        for lens in reversed(position_for_print):
            lens = ' '.join(lens)
            print(index, " ", lens)
            index -= 1
        print(goat_messages.BOARD_LINE)
    else:
        index = 1
        for lens in position_for_print:
            lens = ' '.join(lens)
            print(index, " ", lens[::-1])
            index += 1
        print(goat_messages.BOARD_LINE[::-1])


def return_integer(string):
    integer = None
    while integer is None:
        integer = input(string).strip()
        integer = int(integer) if integer.isdecimal() and \
            INTEGER_FIRST_RESULT_OF_QUESTION <= int(integer) <= INTEGER_SECOND_RESULT_OF_QUESTION else None
    return integer


def start_answer():
    integer = return_integer(goat_messages.CHOOSE_OPERATION)
    if integer == INTEGER_FIRST_RESULT_OF_QUESTION:
        return None, None
    elif integer == INTEGER_SECOND_RESULT_OF_QUESTION:
        color = return_integer(goat_messages.CHOOSE_CHECKERS_COLOR)
        return (HUMANS_MOVE, goat_and_wolves.WHITE_CHECKER) if \
            color == INTEGER_SECOND_RESULT_OF_QUESTION else (COMPUTERS_MOVE, goat_and_wolves.BLACK_CHECKER)


def congratulates(color_human, winner):
    if color_human == winner:
        print(goat_messages.CONGRATULATION)
    else:
        print(goat_messages.YOU_LOOSE)
    return


def piece_of_game(who_moves, color, position):
    while who_moves:
        position = getattr(sys.modules[__name__], who_moves)(position)
        if who_moves == COMPUTERS_MOVE:
            print_board(position, color)
        who_moves = COMPUTERS_MOVE if who_moves == HUMANS_MOVE else HUMANS_MOVE
        if positions_evaluation(position):
            who_moves = None
    return position


def main():
    # власне керує всім процесом.
    while True:
        who_moves, color_for_humans_player = start_answer()
        if who_moves is None:
            return
        position = goat_and_wolves.generate_initial_board()
        print_board(position, color_for_humans_player)
        position = piece_of_game(who_moves, color_for_humans_player, position)
        who_wins = positions_evaluation(position)
        congratulates(color_for_humans_player, who_wins)

if __name__ == '__main__':
    instruction()
    main()
