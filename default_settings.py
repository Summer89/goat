#!/usr/bin/python

__author__ = 'bezobiuk'

#  Цей файл містить дефолтні налаштування

DB_HOST = "localhost"
DB_USERNAME = "root"
DB_PASSWORD = "## пароль створеної вами бази"
DB_NAME = "goat_and_wolves"
session_opts = {
    'session.type': 'file',
    'session.cookie_expires': 864000,
    'session.data_dir': './data',
    'session.auto': True}
PREFIX_TO_URL = ''
