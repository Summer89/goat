#!/usr/bin/python
#  Програма призначена для прорахунку виграшу шашок того чи іншого кольору відомої гри "коза та вовки",
#  з можливістю зміни розмірів дошки. В майбутньому для створення компьютерного гравця.


import MySQLdb
import settings
import goat_messages


ROW = 8
SQUARE = 8
WHITE_CHECKER = 'w'
BLACK_CHECKER = 'b'
WHITE_MOVES = 'W'
BLACK_MOVES = 'B'
WIN_FOR_MOVING_SIDE = 'win'
LOSS_FOR_MOVING_SIDE = 'loss'
UNKNOWN_FOR_MOVING_SIDE = 'n'
EMPTY_SQUARE = 'o'
INDEX_FOR_BOARD = 0
INDEX_FOR_COLOR_MOVES = 1
INDEX_IN_STRING_FOR_COLOR_MOVES = -1
INDEX_FOR_ROW_START_POSITION = 0
INDEX_FOR_SQUARE_START_POSITION = 0
WHITE_DIRECTIONS = ((1, 1), (1, -1), (-1, 1), (-1, -1))  # Для прорахунку можливих ходів білої шашки
BLACK_DIRECTIONS = ((1, -1), (-1, -1))  # Для прорахунку можливих ходів чорних шашок


def make_empty_board(square, row):  # Описує пусту дошку
    empty_board = [[EMPTY_SQUARE] * square for _ in range(row)]
    return empty_board


def make_start_board(empty_board):  # Описує початкову позицію
    empty_board[INDEX_FOR_ROW_START_POSITION][INDEX_FOR_SQUARE_START_POSITION] = WHITE_CHECKER
    row = empty_board[ROW - 1]
    for square in range((ROW - 1) % 2, len(row), 2):
        row[square] = BLACK_CHECKER
    return [empty_board, WHITE_MOVES]


def position_to_string(position):
    return (''.join(''.join(row) for row in position[INDEX_FOR_BOARD])) + position[INDEX_FOR_COLOR_MOVES]


def string_to_position(string_of_position, position=None):
    index = 0
    position = position or make_empty_board(SQUARE, ROW)
    for row in position:
        for square in range(len(row)):
            row[square] = string_of_position[index]
            index += 1
    return [position, string_of_position[index]]


def in_board(x, y):
    return 0 <= x < SQUARE and 0 <= y < ROW


def get_checker_moves(x, y, directions):  # Перевіряє можливість виконання ходу
    return [(x + dx, y + dy) for dx, dy in directions if in_board(x + dx, y + dy)]


def get_possible_moves(string_of_position):  # Обчислює всі можливі ходи з данної позиції
    result = []
    position = string_to_position(string_of_position)
    status_of_board = position[INDEX_FOR_BOARD]
    who_moves = position[INDEX_FOR_COLOR_MOVES]
    for y in range(len(status_of_board)):
        for x in range(len(status_of_board[y])):
            if status_of_board[y][x] == who_moves.lower():
                possible_moves = get_checker_moves(
                    x, y, WHITE_DIRECTIONS if who_moves == WHITE_MOVES else BLACK_DIRECTIONS)
                for new_x, new_y in possible_moves:
                    if status_of_board[new_y][new_x] == EMPTY_SQUARE:
                        status_of_board[new_y][new_x] = status_of_board[y][x]
                        status_of_board[y][x] = EMPTY_SQUARE
                        result.append(position_to_string(
                            [status_of_board, WHITE_MOVES if who_moves == BLACK_MOVES else BLACK_MOVES]
                        ))
                        status_of_board[y][x] = status_of_board[new_y][new_x]
                        status_of_board[new_y][new_x] = EMPTY_SQUARE
    return result


def checkers_index(position, checkers_color):  # Щоб опреділити якого кольору шашка на останній позиції по ходу руху
    argument = position[SQUARE * ROW - 1::-1]  # білої шашки вперед
    index_checker = argument.find(checkers_color) // ROW
    return index_checker


def positions_evaluation(position, positions):  # Застосовується при оцінці позиції (виграш, програш)
    white_index = checkers_index(position, WHITE_CHECKER)
    black_index = checkers_index(position, BLACK_CHECKER)
    if position[INDEX_IN_STRING_FOR_COLOR_MOVES] == BLACK_MOVES:
        if white_index <= black_index:
            return True
    elif position[INDEX_IN_STRING_FOR_COLOR_MOVES] == WHITE_MOVES:
        if not positions and black_index <= white_index:
            return True


from time import monotonic as time
start = time()


def solve(tree, position):  # Власне рекурсивна побудова дерева позицій, та їх оцінка (виграш, програш)
    if not (len(tree) % 10000):  # Для того, щоб було видно, що система не зависла, а рахує
        print(int(time()-start), len(tree))
    win_loss = tree.get(position)
    if win_loss:
        return tree, win_loss
    tree[position] = UNKNOWN_FOR_MOVING_SIDE
    positions = get_possible_moves(position)
    if positions_evaluation(position, positions):
        tree[position] = LOSS_FOR_MOVING_SIDE
        return tree, LOSS_FOR_MOVING_SIDE
    all_loss = True
    for new_position in positions:
        tree, win_loss = solve(tree, new_position)
        if win_loss == LOSS_FOR_MOVING_SIDE:
            all_loss = False
    tree[position] = LOSS_FOR_MOVING_SIDE if all_loss else WIN_FOR_MOVING_SIDE
    return tree, tree[position]


def generate_initial_board():
    board = make_empty_board(SQUARE, ROW)
    board = make_start_board(board)
    string = position_to_string(board)
    return string


def debug_print_board(tree):  # застосовується при тестуванні для наглядності, друкує доску
    counter = 0
    for key in tree:
        position = make_empty_board(SQUARE, ROW)
        index = 0
        for row in range(len(position)):
            for square in range(len(position[row])):
                if row % 2 == square % 2:
                    position[row][square] = key[index]
                else:
                    position[row][square] = '-'
                index += 1
        for lens in reversed(position):
            lens = ''.join(lens)
            print(lens)
        print(key[index], tree[key])
        print('\n')
        counter += 1
    print('Количество позиций =', counter)


def save_tree_position(saving_dict):  # Зберігає результати у базу даних
    db = MySQLdb.connect(host=settings.DB_HOST,
                         user=settings.DB_USERNAME,
                         passwd=settings.DB_PASSWORD,
                         db=settings.DB_NAME)
    cursor = db.cursor()
    for key in saving_dict:
        if cursor.execute((
                goat_messages.SELECT_DATA_POSITION_IN_DB.format(position=key))):
            continue
        else:
            sql_string = (goat_messages.INSERT_DATA.format(position=key, evaluation=saving_dict[key]))
            cursor.execute(sql_string)
    db.commit()
    db.close()

if __name__ == '__main__':
    string_position = generate_initial_board()
    tree, status = solve({}, string_position)
    save_tree_position(tree)
