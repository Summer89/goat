#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'bezobiuk'

from bottle import *
from beaker.middleware import SessionMiddleware
from functools import reduce
from settings import session_opts, PREFIX_TO_URL
import goat_and_wolves
import g_a_w_g


POSSIBLE_MOVE_WHITE_CHECKER = 'wpm'
POSSIBLE_MOVE_BLACK_CHECKER = 'bpm'
WHITE_CHECKER_WILL_MOVE = 'wfm'
BLACK_CHECKER_WILL_MOVE = 'bfm'
SQUARE_AFTER_MOVE = 'sam'
IMAGE_OF_WHITE_CHECKER = 'white_checker.png'
IMAGE_OF_BLACK_CHECKER = 'black_checker.png'
IMAGE_FOR_POSSIBLE_MOVE_WHITE_CHECKER = 'white_checker_possible_move.png'
IMAGE_FOR_POSSIBLE_MOVE_BLACK_CHECKER = 'black_checker_possible_move.png'
IMAGE_FOR_WHITE_CHECKER_WILL_MOVE = 'white_checker_for_move.png'
IMAGE_FOR_BLACK_CHECKER_WILL_MOVE = 'black_checker_for_move.png'
IMAGE_FOR_SQUARE_AFTER_MOVE = 'frame_for_old_move.png'
NAME_FOR_CONGRATULATION_IMAGE = 'you_winn.png'
NAME_FOR_UNFORTUNATELY_IMAGE = 'you_loose.png'
ACTION_SELECT = 'select'
ACTION_MOVE = 'move'
KEY_FOR_OLD_ACTION = 'old_action'
KEY_FOR_ID_FOR_MOVE = 'id_for_move'
KEY_FOR_DICT_MOVES = 'dict_moves'
KEY_FOR_POSITION = 'position'
KEY_FOR_OLD_COORDINATE = 'old_coordinate'
KEY_FOR_COORDINATE = 'coordinate'
KEY_FOR_COLOR = 'color'
KEY_FOR_DICT_IMAGES_FOR_TEMPLATE = 'dict_images_for_template'
KEY_FOR_NAME_IMAGE_RESULT_GAME = 'name_image_result_game'
EXCEPTION_NAME = '//--Defective '
SPACE = ' '
NAME_PAGE_FILE = 'page'
NAME_START_PAGE_FILE = 'start_page'
NAME_ERROR_PAGE = 'error_page'
START_VALUE_FOR_ID = 0
INDEX_KEY_FOR_UPDATING = 0
INDEX_FOR_IMAGE = 0
INDEX_FOR_ACTION = 1
LEN_TUPLE_WITH_FULL_DATA = 2
INDEX_FOR_VALUE = 0
STEP_FOR_INDEX = 1
INDEX_FOR_FIRST_VALUE = 0
INDEX_FOR_SECOND_VALUE = 1
NAMES_IMAGES = {goat_and_wolves.BLACK_CHECKER: IMAGE_OF_BLACK_CHECKER,
                goat_and_wolves.WHITE_CHECKER: IMAGE_OF_WHITE_CHECKER,
                POSSIBLE_MOVE_WHITE_CHECKER: IMAGE_FOR_POSSIBLE_MOVE_WHITE_CHECKER,
                POSSIBLE_MOVE_BLACK_CHECKER: IMAGE_FOR_POSSIBLE_MOVE_BLACK_CHECKER,
                WHITE_CHECKER_WILL_MOVE: IMAGE_FOR_WHITE_CHECKER_WILL_MOVE,
                BLACK_CHECKER_WILL_MOVE: IMAGE_FOR_BLACK_CHECKER_WILL_MOVE,
                SQUARE_AFTER_MOVE: IMAGE_FOR_SQUARE_AFTER_MOVE}
DATA_FOR_SAFE_ROUTE = {KEY_FOR_COLOR: (goat_and_wolves.WHITE_CHECKER, goat_and_wolves.BLACK_CHECKER),
                       KEY_FOR_OLD_ACTION: (ACTION_SELECT, ACTION_MOVE)}

_app = Bottle()
app = application = SessionMiddleware(_app, session_opts)


@_app.route(PREFIX_TO_URL + '/g_a_w_g/staticfiles/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='./')


def data_for_template(dict_coordinates):
    # в словнику для генерації темплейта ключем є координати клітинки з даними, а даними являється кортеж з імені
    # картинки, яка генерується на клітинці та з строки дії, яка потім підставляється в url ссилки, якщо ссилка
    # потрібна на даній клітинці
    dict_images_for_template = {}
    for key in dict_coordinates:
        if len(dict_coordinates[key]) != LEN_TUPLE_WITH_FULL_DATA:
            dict_coordinates[key] = (dict_coordinates[key][INDEX_FOR_IMAGE], None)
        dict_images_for_template[key] = (NAMES_IMAGES[dict_coordinates[key][INDEX_FOR_IMAGE]],
                                         dict_coordinates[key][INDEX_FOR_ACTION])
    return dict_images_for_template


def data_for_result_game_template(position_for_evaluation, session, id_for_move, coordinate, beginning_of_key):
    # функція викликає функцію оцінки позиції, яка повертає або колір шашки, що виграє, або None
    # якщо позиція виграшна, то відповідно вставляється привітання,або співчуття, та генерується
    # dict_images_for_template без ссилок
    who_wins = g_a_w_g.positions_evaluation(position_for_evaluation)
    dict_coordinates_squares = g_a_w_g.convert_string(position_for_evaluation)
    dict_images_for_template = data_for_template(dict_coordinates_squares)
    if who_wins:
        result = NAME_FOR_CONGRATULATION_IMAGE if who_wins == session[KEY_FOR_COLOR] else NAME_FOR_UNFORTUNATELY_IMAGE
        data_for_update_session = ((KEY_FOR_DICT_IMAGES_FOR_TEMPLATE, dict_images_for_template),
                                   (KEY_FOR_NAME_IMAGE_RESULT_GAME, result),
                                   (KEY_FOR_ID_FOR_MOVE, id_for_move),
                                   (KEY_FOR_OLD_COORDINATE, coordinate),
                                   (KEY_FOR_POSITION, position_for_evaluation))
        beginning_of_key.update(data_for_update_session)
        return dict_images_for_template, result
    else:
        return None, None


def data_for_initial_position(session, id_move, id_for_move, color):
    session[KEY_FOR_DICT_MOVES] = {}
    beginning_of_key = session[KEY_FOR_DICT_MOVES][id_move] = {}
    data_for_update_session = ((KEY_FOR_ID_FOR_MOVE, id_for_move),
                               (KEY_FOR_COORDINATE, beginning_of_key.get(KEY_FOR_COORDINATE)))
    beginning_of_key.update(data_for_update_session)
    session[KEY_FOR_COLOR] = color
    action = ACTION_SELECT
    cursor, db = g_a_w_g.open_db_of_moves()
    position = goat_and_wolves.generate_initial_board() if color == goat_and_wolves.WHITE_CHECKER else \
        g_a_w_g.computers_move(goat_and_wolves.generate_initial_board())
    g_a_w_g.close_db_of_moves(db)
    dict_coordinates_squares = g_a_w_g.convert_string(position)
    if color == goat_and_wolves.BLACK_CHECKER:
        changed_coordinates = set(g_a_w_g.convert_string(goat_and_wolves.generate_initial_board())) ^\
            set(g_a_w_g.convert_string(position))
        old_changed_coordinates = set(g_a_w_g.convert_string(goat_and_wolves.generate_initial_board())) -\
            set(g_a_w_g.convert_string(position))
        for coordinate in old_changed_coordinates:
            dict_coordinates_squares[coordinate] = (SQUARE_AFTER_MOVE, )
        for coordinate in changed_coordinates - old_changed_coordinates:
            dict_coordinates_squares[coordinate] = (WHITE_CHECKER_WILL_MOVE, )
    for coordinate in dict_coordinates_squares:
        if dict_coordinates_squares[coordinate][INDEX_FOR_IMAGE] == session[KEY_FOR_COLOR]:
            dict_coordinates_squares[coordinate] = (dict_coordinates_squares[coordinate][INDEX_FOR_IMAGE], action)
    dict_images_for_template = data_for_template(dict_coordinates_squares)
    data_for_update_session = ((KEY_FOR_POSITION, position),
                               (KEY_FOR_DICT_IMAGES_FOR_TEMPLATE, dict_images_for_template),
                               (KEY_FOR_NAME_IMAGE_RESULT_GAME, None),
                               (KEY_FOR_OLD_COORDINATE, None))
    beginning_of_key.update(data_for_update_session)
    session[KEY_FOR_POSITION] = position
    session[KEY_FOR_OLD_COORDINATE] = None
    session.save()
    return dict_images_for_template


def data_for_select_action(session, id_move, id_for_move, coordinate, old_action):
    action = ACTION_MOVE
    beginning_of_key = session[KEY_FOR_DICT_MOVES][id_move] = {}
    position = session[KEY_FOR_POSITION]
    session[KEY_FOR_OLD_COORDINATE] = coordinate
    possible_moves = goat_and_wolves.get_possible_moves(position)
    dict_possible_moves = [d_move for move in possible_moves for d_move in
                           (g_a_w_g.convert_string(move),) if coordinate not in d_move]
    dict_coordinates_squares = g_a_w_g.convert_string(position)
    for key in dict_coordinates_squares:
        if dict_coordinates_squares[key][INDEX_FOR_IMAGE] == session[KEY_FOR_COLOR]:
            dict_coordinates_squares[key] = (dict_coordinates_squares[key][INDEX_FOR_IMAGE], old_action)
    dict_possible_moves.append(dict_coordinates_squares)
    destination_coordinates = reduce(lambda s1, s2: s1 ^ s2,
                                     [set(move.keys()) for move in dict_possible_moves]) -\
        set(dict_coordinates_squares)
    for destination_coordinate in destination_coordinates:
        is_white_checker = session[KEY_FOR_COLOR] == goat_and_wolves.WHITE_CHECKER
        dict_coordinates_squares[destination_coordinate] = \
            (POSSIBLE_MOVE_WHITE_CHECKER if is_white_checker else POSSIBLE_MOVE_BLACK_CHECKER, action)
        dict_coordinates_squares[coordinate] = \
            (WHITE_CHECKER_WILL_MOVE if is_white_checker else BLACK_CHECKER_WILL_MOVE, )
    dict_images_for_template = data_for_template(dict_coordinates_squares)
    data_for_update_session = ((KEY_FOR_OLD_COORDINATE, coordinate),
                               (KEY_FOR_ID_FOR_MOVE, id_for_move),
                               (KEY_FOR_COORDINATE, coordinate),
                               (KEY_FOR_POSITION, position),
                               (KEY_FOR_DICT_IMAGES_FOR_TEMPLATE, dict_images_for_template),
                               (KEY_FOR_NAME_IMAGE_RESULT_GAME, None))
    beginning_of_key.update(data_for_update_session)
    session[KEY_FOR_POSITION] = position
    session.save()
    return dict_images_for_template, None


def data_for_move_action(session, id_move, id_for_move, coordinate):
    action = ACTION_SELECT
    beginning_of_key = session[KEY_FOR_DICT_MOVES][id_move] = {}
    old_position = session[KEY_FOR_POSITION]
    move = session[KEY_FOR_OLD_COORDINATE] + '-' + coordinate
    position, massage = g_a_w_g.piece_of_humans_move(old_position, move)
    # Змінна massage тут не приміняється, вона необхідна тільки в модулі g_a_w_g.py
    dict_images_for_template, name_image_result_game =\
        data_for_result_game_template(position, session, id_for_move, coordinate, beginning_of_key)
    if name_image_result_game:
        return dict_images_for_template, name_image_result_game
    cursor, db = g_a_w_g.open_db_of_moves()
    new_position = g_a_w_g.computers_move(position)
    dict_images_for_template, name_image_result_game =\
        data_for_result_game_template(new_position, session, id_for_move, coordinate, beginning_of_key)
    if name_image_result_game:
        return dict_images_for_template, name_image_result_game
    g_a_w_g.close_db_of_moves(db)
    changed_coordinates = (set(g_a_w_g.convert_string(old_position)) ^ set(g_a_w_g.convert_string(position))) |\
                          (set(g_a_w_g.convert_string(position)) ^ set(g_a_w_g.convert_string(new_position)))
    dict_coordinates_squares = g_a_w_g.convert_string(new_position)
    old_computers_coordinate = set(g_a_w_g.convert_string(position)) - set(g_a_w_g.convert_string(new_position))
    new_computers_coordinate = set(g_a_w_g.convert_string(new_position)) - set(g_a_w_g.convert_string(position))
    for coordinate in dict_coordinates_squares:
        if dict_coordinates_squares[coordinate][INDEX_FOR_IMAGE] == session[KEY_FOR_COLOR]:
            dict_coordinates_squares[coordinate] = \
                (dict_coordinates_squares[coordinate][INDEX_FOR_IMAGE], action)
    for key in dict_coordinates_squares.keys():
        if key in changed_coordinates:
            dict_coordinates_squares[key] = (WHITE_CHECKER_WILL_MOVE, action) if \
                (dict_coordinates_squares[key][INDEX_FOR_IMAGE] == goat_and_wolves.WHITE_CHECKER
                 == session[KEY_FOR_COLOR]) else (BLACK_CHECKER_WILL_MOVE, action) if \
                (dict_coordinates_squares[key][INDEX_FOR_IMAGE] == goat_and_wolves.BLACK_CHECKER
                 == session[KEY_FOR_COLOR]) else dict_coordinates_squares[key]
        if key in new_computers_coordinate:
            is_white_checker = dict_coordinates_squares[key][INDEX_FOR_IMAGE] == goat_and_wolves.WHITE_CHECKER
            dict_coordinates_squares[key] = \
                (WHITE_CHECKER_WILL_MOVE if is_white_checker else BLACK_CHECKER_WILL_MOVE, )
    for coordinate in old_computers_coordinate:
        dict_coordinates_squares[coordinate] = (SQUARE_AFTER_MOVE, )
    dict_images_for_template = data_for_template(dict_coordinates_squares)
    data_for_update_session = ((KEY_FOR_ID_FOR_MOVE, id_for_move),
                               (KEY_FOR_COORDINATE, coordinate),
                               (KEY_FOR_POSITION, new_position),
                               (KEY_FOR_DICT_IMAGES_FOR_TEMPLATE, dict_images_for_template),
                               (KEY_FOR_NAME_IMAGE_RESULT_GAME, None),
                               (KEY_FOR_OLD_COORDINATE, None))
    beginning_of_key.update(data_for_update_session)
    session[KEY_FOR_POSITION] = new_position
    session.save()
    return dict_images_for_template, None


def data_for_refresh_safe_position(session, id_move, id_for_move):
    beginning_of_key = session[KEY_FOR_DICT_MOVES][id_move]
    session[KEY_FOR_OLD_COORDINATE] = beginning_of_key[KEY_FOR_OLD_COORDINATE]
    session[KEY_FOR_POSITION] = beginning_of_key[KEY_FOR_POSITION]
    dict_images_for_template = beginning_of_key[KEY_FOR_DICT_IMAGES_FOR_TEMPLATE]
    name_image_result_game = beginning_of_key[KEY_FOR_NAME_IMAGE_RESULT_GAME]
    color_human_checkers = session[KEY_FOR_COLOR]
    session.save()
    id_for_move = int(id_for_move) + STEP_FOR_INDEX
    return dict_images_for_template, name_image_result_game, id_for_move, color_human_checkers


def data_for_go_back_save_position(session, id_for_move):
    keys_for_deleting = [key for key in session[KEY_FOR_DICT_MOVES] if
                         int(session[KEY_FOR_DICT_MOVES][key][KEY_FOR_ID_FOR_MOVE]) >= int(id_for_move)]
    keys_for_updating = [key for key in session[KEY_FOR_DICT_MOVES] if
                         int(session[KEY_FOR_DICT_MOVES][key][KEY_FOR_ID_FOR_MOVE])
                         == int(id_for_move) - STEP_FOR_INDEX][INDEX_KEY_FOR_UPDATING]
    beginning_of_key = session[KEY_FOR_DICT_MOVES][keys_for_updating]
    session[KEY_FOR_OLD_COORDINATE] = beginning_of_key[KEY_FOR_COORDINATE]
    session[KEY_FOR_POSITION] = beginning_of_key[KEY_FOR_POSITION]
    for key in keys_for_deleting:
        del(session[KEY_FOR_DICT_MOVES][key])
    session.save()
    return


def safe_route(func):
    # функція обробляє помилки.
    def new_func(*_args, **_kwargs):
        try:
            for key in DATA_FOR_SAFE_ROUTE:
                if _kwargs.get(key):
                    if _kwargs.get(key) in DATA_FOR_SAFE_ROUTE[key]:
                        pass
                    else:
                        raise Exception(EXCEPTION_NAME+key+SPACE+_kwargs.get(key))
            return func(*_args, **_kwargs)
        except Exception as e:
            print(func.__name__, e)
        return template(NAME_ERROR_PAGE, prefix=PREFIX_TO_URL)
    return new_func


@_app.route(PREFIX_TO_URL + '/g_a_w_g/')
@safe_route
def generate_start_page():
    id_for_move = START_VALUE_FOR_ID
    return template(NAME_START_PAGE_FILE, id_for_move=id_for_move, prefix=PREFIX_TO_URL)


@_app.route(PREFIX_TO_URL + '/g_a_w_g/<id_for_move>/<color>')
@safe_route
def generate_initial_position_page(id_for_move=None, color=None):
    id_move = id_for_move + color
    session = request.environ.get('beaker.session')
    dict_images_for_template = data_for_initial_position(session, id_move, id_for_move, color)
    id_for_move = int(id_for_move) + STEP_FOR_INDEX
    return template(NAME_PAGE_FILE, dict_images_for_template=dict_images_for_template, prefix=PREFIX_TO_URL,
                    color_human_checkers=color, name_image_result_game=None, id_for_move=id_for_move)


@_app.route(PREFIX_TO_URL + '/g_a_w_g/<id_for_move>/<coordinate>/<old_action>')
@safe_route
def generate_position_of_action_page(coordinate=None, old_action=None, id_for_move=None):
    # Власне функція всім керує. Вона створює словник із даними для генерації темплейта та реалізує його.
    # Словник створюється на основі даних, які приходять від користувача і для кожної ситуації він різний.
    # генеруються рисунки обєктів по координатах, а також ссилки. Ссилки генеруються тільки на тих обєктах на
    # яких вони в даній ситуації можуть бути необхідні. Тобто користувач не натисне нічого лишнього.
    # Колір шашок, якими грає користувач передається для того, щоб коректно відтворювати дошку на екрані відносно
    # користувача. Для рефреша та повернення браузера кожен хід зберігається в словнику в сесії, де ключем є строка
    # утворена числом(id_for_move) + координата + дія. Перед кожним ходом перевіряється, чи вже був такий хід, і
    # якщо був, то дані дістаються із словника, а якщо ні то все по ходу програми.
    name_image_result_game = None
    dict_images_for_template = {}
    session = request.environ.get('beaker.session')
    session[KEY_FOR_DICT_MOVES] = session.get(KEY_FOR_DICT_MOVES, {})
    id_move = id_for_move + coordinate + old_action
    id_all_moves = [session[KEY_FOR_DICT_MOVES][key][KEY_FOR_ID_FOR_MOVE] for key in session[KEY_FOR_DICT_MOVES]]
    if session[KEY_FOR_DICT_MOVES] and id_move in session[KEY_FOR_DICT_MOVES]:
        dict_images_for_template, name_image_result_game, id_for_move, color_human_checkers =\
            data_for_refresh_safe_position(session, id_move, id_for_move)
        return template(NAME_PAGE_FILE, dict_images_for_template=dict_images_for_template, prefix=PREFIX_TO_URL,
                        color_human_checkers=color_human_checkers, name_image_result_game=name_image_result_game,
                        id_for_move=id_for_move)
    elif session[KEY_FOR_DICT_MOVES] and id_move not in session[KEY_FOR_DICT_MOVES] and id_for_move in id_all_moves:
        data_for_go_back_save_position(session, id_for_move)
    if old_action == ACTION_SELECT:
        dict_images_for_template, name_image_result_game =\
            data_for_select_action(session, id_move, id_for_move, coordinate, old_action)
    elif old_action == ACTION_MOVE:
        dict_images_for_template, name_image_result_game = \
            data_for_move_action(session, id_move, id_for_move, coordinate)
    id_for_move = int(id_for_move) + STEP_FOR_INDEX
    return template(NAME_PAGE_FILE, dict_images_for_template=dict_images_for_template, prefix=PREFIX_TO_URL,
                    color_human_checkers=session[KEY_FOR_COLOR],
                    name_image_result_game=name_image_result_game, id_for_move=id_for_move)

if __name__ == '__main__':
    run(app=app, host='localhost', port=8080)
