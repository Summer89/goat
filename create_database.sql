CREATE DATABASE goat_and_wolves;
USE goat_and_wolves;
CREATE TABLE gaw_positions
	(
		gp_id INT NOT NULL AUTO_INCREMENT,
		gp_positions VARCHAR(110),
		gp_evaluations VARCHAR(4),
		PRIMARY KEY (gp_id),
		UNIQUE KEY (gp_positions)
	);
