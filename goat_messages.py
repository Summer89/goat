__author__ = 'bezobiuk'

INSTRUCTION = '''\t\t   Після запуску програми  зачекайте  декілька  секунд поки програма
           завантажить усі свої файли.
           Програма графічно друкує вигляд ігрової дошки після кожного ходу.
           Для  вводу  вашого  ходу необхідно ввести  координати  шашки, яка
           має здійснити хід, та після вводу  одного  з  розділових  знаків,
           такого  як (-,:;.=),  ввести  координати  куди  перенести  шашку.
           Ввод розділового  знаку обов\'язковий.  Регістр вводу  значення не
           має. Приклад ходу: g8-h7. Бажаю Вам перемоги у грі!'''
YOUR_MOVE = 'Зробіть свій хід:\n'
CHECK_YOUR_MOVE = 'Перевірте будь- ласка правильність вводу вашого ходу.'
IMPOSSIBLE_MOVE = 'Перенести шашку в указане місце неможливо.'
CHECK_CHECKER = 'Виберіть свою шашку.'
CONGRATULATION = 'Вітаю ви виграли, можливо спробуєте ще?'
YOU_LOOSE = 'Співчуваю, але ви програли, можливо варто спробувати ще?'
CHOOSE_CHECKERS_COLOR = '''Виберіть колір шашок:
            0 - чорні
            1 - білі \n'''
CHOOSE_OPERATION = '''Виберіть дію:
            0 - вийти
            1 - зіграти партію \n'''
BOARD_LINE = '\n    A B C D E F G H    \n'
SELECT_DATA = "SELECT gp_evaluations FROM gaw_positions WHERE gp_positions = '{position}';"
SELECT_DATA_POSITION_IN_DB = "SELECT gp_positions FROM gaw_positions WHERE gp_positions = '{position}';"
INSERT_DATA = "INSERT INTO gaw_positions VALUES (NULL, '{position}', '{evaluation}');"
