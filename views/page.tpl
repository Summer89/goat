<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="шашки, игра, коза, волк, овца, гра, вівця, вовк, game, checkers, board game, goose, fox, wolf">
    <title>Гра в козу та вовків</title>
    <link rel="stylesheet" type="text/css" href="{{prefix}}/g_a_w_g/staticfiles/css/style.css"/>
</head>
<body>
	% if name_image_result_game:
		<div class = 'image_result_game'><img img id = 'image_result_game' src='{{prefix}}/g_a_w_g/staticfiles/images/{{name_image_result_game}}'></div>
    % else:
		<div class = 'none'></div>
    % end
    <div class="full_board">
    % literals = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    % INDEX_FOR_IMAGE = 0
    % INDEX_FOR_ACTION = 1
    % WHITE_CHECKER = 'w'
	% BLACK_CHECKER = 'b'
    % if color_human_checkers == WHITE_CHECKER:
    	% numerals = 8
    	% range_numbers = 0
    	% step_iterables = -1
    % elif color_human_checkers == BLACK_CHECKER:
    	% numerals = 1
    	% range_numbers = 9
    	% step_iterables = 1
    	% literals = literals[::-1]
    % end
        <div id="column_numbers" class="outside">
        	% for number in range(numerals, range_numbers, step_iterables):
        		<div id="ln_{{number}}" class="numbers">{{number}}</div>
            % end    
        </div>
        <div class="board">
            % for literal in literals:
                <div id="column_{{literal}}" class="column">
                    % indicator = literals.index(literal)
                    % for number in range(numerals, range_numbers, step_iterables):
                    	% if color_human_checkers == WHITE_CHECKER:
                        	% class_of_square = 'white_square' if (number%2 and indicator%2) or (not number%2 and not indicator%2) else 'black_square'
                        % elif color_human_checkers == BLACK_CHECKER:
                        	% class_of_square = 'white_square' if (number%2 and not indicator%2) or (not number%2 and indicator%2) else 'black_square'
                        % end
                        % coordinates = literal + str(number)
                        % if coordinates in dict_images_for_template:
                            % url_for_image ="{prefix}/g_a_w_g/staticfiles/images/{image}".format(prefix = prefix, image = dict_images_for_template[coordinates][INDEX_FOR_IMAGE])
                            % if dict_images_for_template[coordinates][INDEX_FOR_ACTION]:
                            	% url_for_href = "{prefix}/g_a_w_g/{id_for_move}/{coordinates}/{action}". format(prefix = prefix, id_for_move = id_for_move, coordinates = coordinates, action = dict_images_for_template[coordinates][INDEX_FOR_ACTION])
                            	<div id="{{literal}}{{number}}" class="{{class_of_square}}">
                            	<a href="{{url_for_href}}"><img src="{{url_for_image}}"></a></div>
                            % else:
                            	<div id="{{literal}}{{number}}" class="{{class_of_square}}">
                            	<img src="{{url_for_image}}"></div>
                            % end
                        % else:
                            <div id="{{literal}}{{number}}" class="{{class_of_square}}"></div>
                        % end
                    % end
                </div>
            % end
        </div>
            <div id="line_literals" class="bottom">
                % for literal in literals:
                    <div id="cn_{{literal}}" class="literal">{{literal}}</div>
                % end
            </div>
    % end
    </div>
    <div class = 'picture_on_table'><img img id = 'picture_on_table' src='{{prefix}}/g_a_w_g/staticfiles/images/picture_on_table.gif'></div>
    <div class = 'new_game_link'><a href="{{prefix}}/g_a_w_g/"><img id = 'new_game_image' onmouseover="this.src='{{prefix}}/g_a_w_g/staticfiles/images/new_game_active_link.png ';" onmouseout="this.src='{{prefix}}/g_a_w_g/staticfiles/images/new_game.png ';" src='{{prefix}}/g_a_w_g/staticfiles/images/new_game.png'></a></div>
    <div id="copyright">&copy; Bezobiuk Igor 2016</div>

</body>
</html>