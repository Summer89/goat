<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="шашки, игра, коза, волк, овца, гра, вівця, вовк, game, checkers, board game, goose, fox, wolf">
    <title>Гра в козу та вовків</title>
    <link rel="stylesheet" type="text/css" href="{{prefix}}/g_a_w_g/staticfiles/css/style.css"/>
</head>
<body>
	<div class = 'title_image'><img id = 'title_image' src='{{prefix}}/g_a_w_g/staticfiles/images/title.png'></div>
	<div class = 'wolf'><a href="{{prefix}}/g_a_w_g/{{id_for_move}}/b"><img id = 'wolf' onmouseover="this.src='{{prefix}}/g_a_w_g/staticfiles/images/wolf_active_href.gif';" onmouseout="this.src='{{prefix}}/g_a_w_g/staticfiles/images/wolf.gif';" src="{{prefix}}/g_a_w_g/staticfiles/images/wolf.gif"></a></div>
	<div class = 'goat'><a href="{{prefix}}/g_a_w_g/{{id_for_move}}/w"><img id = 'goat' onmouseover="this.src='{{prefix}}/g_a_w_g/staticfiles/images/goat_active_href.gif';" onmouseout="this.src='{{prefix}}/g_a_w_g/staticfiles/images/goat.gif';" src="{{prefix}}/g_a_w_g/staticfiles/images/goat.gif"></a></div>
	<div class = 'rules_link'><a href="https://ru.wikipedia.org/wiki/%D0%92%D0%BE%D0%BB%D0%BA_%D0%B8_%D0%BE%D0%B2%D1%86%D1%8B" target="_blank"><img id = 'rules_image' onmouseover="this.src='{{prefix}}/g_a_w_g/staticfiles/images/rules_image_active_link.png';" onmouseout="this.src='{{prefix}}/g_a_w_g/staticfiles/images/rules_image.png';" src='{{prefix}}/g_a_w_g/staticfiles/images/rules_image.png'></a></div>
    <div id="copyright">&copy; Bezobiuk Igor 2016</div>
</body>
</html>